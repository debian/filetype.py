#!/usr/bin/python3

import filetype

def main():
    kind = filetype.guess('debian/tests/lua.jpg')
    if kind is None:
        print('Cannot guess file type!')
        return

    print('File extension: {}'.format(kind.extension))
    print('File MIME type: {}'.format(kind.mime))


if __name__ == '__main__':
    main()
